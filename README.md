# Hi! Welcome to my profile!  
Hey! my name is Sabzi Greenly.
I'm a simple coder&developer.
I love freedom and free-softwares.
In my past life, I was coding in python and JS. But right now I'm just learning [Rust](https://www.rust-lang.org/) and try to code with this.
Yeah. Now my favorite programming language is Rust.
I love [Gnu/Linux](https://www.gnu.org) operating system and my favorite distributions are: [Gentoo](https://www.gentoo.org/), [ArchLinux](https://archlinux.org/) and [AlpineLinux](https://www.alpinelinux.org).
I almost listen to hard rock and heavy metal musics.
Yeap. It's me. I'm Sabzi! :)
Also I love you all. <3
### Contact with me 

- [Gitlab](https://gitlab.com/greenly)
- [Mastodon](https://mastodon.online/@sabzi)
- <javadi@tutanota.com>